<?php

namespace symfony\fixture;

use symfony\entity\quote;
use Doctrine\Bundle\FixturesBundle\fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Address;

class quoteFixture extends fixture
{
    private $f;


    public function __construct() 
	{
        $a->f = Factory::create();
    }
    public function load(ObjectManager $m): void
    {
        for($i = 0; $i < 50; $i++) 
		{
            $m->persist($a->getQ());
        }

        $m->flush();
    }

    private function getQ() : Q 
	{
        return new Q
		(
            $a->f->sentence(10),
            $a->f->name(),
            $a->f->d()
        );
    }
}
