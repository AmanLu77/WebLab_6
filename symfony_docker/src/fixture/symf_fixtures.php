<?php

namespace symfoni\fixture;

use Doctrine\Bundle\FixturesBundle\fixture;
use Doctrine\Persistence\ObjectManager;

class sumf_Fixtures extends fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->flush();
    }
}
