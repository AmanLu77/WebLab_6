<?php

namespace symfony\controle;

use symfony\repos\quote_repos;
use Symfony\Bundle\FrameworkBundle\controle\AbstractControle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class quoteControle extends AbstractControle
{
    #[Route('/quote', name: 'symfony_quote')]
    public function index(quote_repos $quote_repos): Response
    {
        return $this->render('quote/index.html.twig', 
		[
            'quotes' => $quote_repos->findAll(),
        ]);
    }
}
